title: GitLab Ultimate for IBM Cloud Paks
canonical_path: /partners/technology-partners/ibm/
seo_title: GitLab Ultimate for IBM Cloud Paks
description: Accelerate innovation with an open DevOps solution. Simplify how software is developed, delivered, and managed with technologies from GitLab and IBM.
title_description: Accelerate innovation with an open DevOps solution. Simplify how software is developed, delivered, and managed with technologies from GitLab and IBM.
logo: /images/applications/apps/ibm.png
logo_alt: IBM Logo
body_top_title: Discover efficiency, deliver results
body_top_description: Increase productivity with agile collaboration and optimize delivery with data-driven automation. GitLab Ultimate provides an open DevOps platform for delivering better hybrid cloud software faster with IBM Cloud Paks.
body_top_cta_1_text: Read the 451 report
body_top_cta_2_text: Learn more
body_top_cta_img_1: "/images/topics/featured-partner-ibm-1.svg"
body_top_cta_img_2: "/images/topics/featured-partner-ibm-2.svg"
body_top_cta_1_url: https://learn.gitlab.com/451-ibm-gitlab-aiops-devops/451-ibm-oem
body_top_cta_2_url: https://www.ibm.com/products/gitlab-ultimate
body_features_title: Automate your enterprise workloads to deliver better applications faster
body_features_description: For organizations to rapidly shift workloads to the cloud, they need a secure, portable method without vendor lock-in. GitLab Ultimate for IBM Cloud Paks is designed to help project teams that want to deploy an application to different systems located on environments such as IBM Cloud, IBM Z, or bare metal servers, as a single, comprehensive hybrid cloud solution.
body_quote: Key use cases for the new product include efficient and secure automation of software development, delivery, and management, and the use of AI to accelerate DevOps by making processes and responses more intelligent.
body_quote_source: Jay Lyman, 451 Research (2021)
feature_items:
  - title: "Productive pipelines"
    description: "GitLab Ultimate for IBM Cloud Paks offers a standardized pipeline that builds on the CI/CD, application security testing, source control, and single-user experience of GitLab with out-of-the-box extensions for service virtualization, integration testing, release orchestration, and scalable governance."
    icon: "/images/enterprise/gitlab-enterprise-icon-smiley-face.svg"
  - title: "Integrated DevSecOps"
    description: "Together, IBM and GitLab help clients streamline collaboration to improve productivity and accelerate innovation in the environment of their choosing. Discover an integrated DevSecOps approach with extensions for other tools."
    icon: "/images/enterprise/gitlab-enterprise-icon-smiley-face.svg"
  - title: "Unparalleled automation"
    description: "GitLab Ultimate is integrated with IBM Cloud Paks, IBM Watson AIOps, and IBM Z to help teams become more agile and efficient. With GitLab Ultimate for IBM Cloud Paks, companies can automate work across business users, developers, and IT teams."
    icon: "/images/enterprise/gitlab-enterprise-icon-smiley-face.svg"
body_middle_title: "Get started with GitLab and IBM"
body_middle_description: >-
  From centralized management to cost reduction – GitLab for IBM Cloud Paks, Watson, and IBM Z offer a joint solution that streamlines hybrid software delivery across multiple pipelines.
body_middle_items:
  - title: "GitLab + IBM Cloud Paks"
    description: "Leverage AI-powered software built on Red Hat OpenShift for hybrid clouds. With GitLab and IBM Cloud Paks, fully implement intelligent workflows to accelerate your business’s digital transformation."
    icon: "/images/icons/icon-1.svg"
    link: "https://www.ibm.com/cloud/paks?lnk=hpmps_bupr"
  - title: "GitLab + IBM Watson AIOps"
    description: "The IBM Watson AIOps and GitLab integration automate the work of site reliability engineers (SREs). Ingest data and events from GitLab across the software development lifecycle – including code pushes, commits, reviews, tests, deployments, and releases."
    icon: "/images/icons/icon-2.svg"
    link: "https://www.ibm.com/watson?lnk=hpmps_bupr"
  - title: "GitLab + IBM System Z: GitLab + IBM Dependency Based Build"
    description: "GitLab Ultimate for IBM Cloud Paks bundles with IBM Dependency Based Build to provide a DevOps platform that includes the z/OS team. Named GitLab Ultimate for IBM z/OS, this is an enterprise-wide DevOps solution."
    icon: "/images/icons/icon-3.svg"
    link: "https://www.ibm.com/cloud/blog/announcements/gitlab-ultimate-for-ibm-z/os"
 
featured_video_title: "Opening Keynote: The Power of GitLab - Sid Sijbrandij"
featured_video_url: "https://www.youtube-nocookie.com/embed/xn_WP4K9dl8"
featured_video_topic: "GitLab Commit Virtual 2020"
featured_video_more_url: "https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg/featured"
benefits_title: Discover the benefits of GitLab on IBM
resources:
  - title: An overview of GitLab Ultimate for IBM Cloud Paks
    url: https://community.ibm.com/HigherLogic/System/DownloadDocumentFile.ashx?DocumentFileKey=58a6cbe3-8cb1-2683-8ee6-913879f285bd&forceDialog=0
    type: Whitepapers
  - title: GitLab's journey on Linux on IBM Z and Red Hat OpenShift
    url: https://about.gitlab.com/blog/2020/09/17/gitlab-and-workloads-on-ibm-z-and-red-hat-openshift/
    type: Blog
  - title: 'IBM GitLab Utlimate and IBM Z: Solution Brief'
    url: https://www.ibm.com/downloads/cas/0AD4LDAO
    type: Whitepapers   
  - title: 'IBM GitLab Utlimate and IBM Z: Podcast with Rosalind Radcliffe'
    url: https://community.ibm.com/community/user/wasdevops/blogs/laurel-dickson-bull1/2021/03/22/ibm-gitlab-utlimate-for-zos-podcast-with-rosalind
    type: Blog
  - title: Expanding our DevOps portfolio with GitLab Ultimate for IBM Cloud Paks
    url: https://www.ibm.com/blogs/systems/expanding-our-devops-portfolio-with-gitlab-ultimate-for-ibm-cloud-paks/
    type: Blog
  - title: The Importance of DevOps Automation
    url: https://www.ibm.com/cloud/blog/importance-of-devops-automation
    type: Blog
  - title: Automating the Enterprise One Idea at a time
    url: https://www.ibm.com/cloud/blog/announcements/automating-the-enterprise-one-idea-at-a-time-ibm-and-gitlab
    type: Blog
  - title: What does IBM partnership with GitLab mean for Rational test
    url: https://community.ibm.com/community/user/wasdevops/blogs/suhas-kashyap1/2021/01/14/what-does-ibm-partnership-with-gitlab-mean-for-rat
    type: Blog
  - title: GitLab and UrbanCode
    url: https://community.ibm.com/community/user/wasdevops/viewdocument/deploy-complex-applications-with-gi?CommunityKey=0ab505af-8e12-4199-843b-0dbbb3848f0e
    type: Blog
  - title: 'Customer Success Webinar: OneMain Financial: GitLab and DBB on IBM Z'
    url: https://event.on24.com/eventRegistration/EventLobbyServlet?target=reg20.jsp&referrer=https%3A%2F%2Fnotes.services.box.com%2F&eventid=3177900&sessionid=1&key=8A3DC08B29BCA630D9B9AB2050D97333&regTag=&V2=false&sourcepage=register
    type: Webcast
cta_banner:
  - title: How can GitLab integrate with your IBM infrastructure?
    button_url: https://about.gitlab.com/sales/
    button_text: Talk to an expert       
