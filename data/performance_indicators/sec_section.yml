- name: Sec - Section CMAU - Reported Sum of Sec Section SMAU
  base_path: "/handbook/product/performance-indicators/"
  definition: A sum of all SMAUs across all stages in the Sec Section (Secure, Protect) for All users and Paid users.
  target: 10% higher than current month (SaaS and self-managed combined)
  org: Sec Section
  section: Sec
  public: true
  pi_type: Section CMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - currently instrumented to meet OKRs (see instrumentation, Lessons Learned for more details)
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free-to-OSS/education Gold/Ultimate users and paid Gold/Ultimate users
    - Threat Insights data is not yet included in this count until we support user-level metrics in Snowplow for self-managed
  lessons:
    learned:
      - Current Self-Managed Uplift is leading to massive underreporting as it doesn't fully account for non-reporting instances (offline environments, security conscious enterprises)
  monthly_focus:
    goals:
      - Overall growth was reported as flat. This is in opposition in our Ultimate growth during this period. As such, this will need to be investigated to confirm if there was data loss.
      - Identify a way to denote underreporting on Sec-related *MAU charts to remove confusion / clarify metrics interpretation
      - Work with the Data Analytics team on improving Self-Managed Uplift for Sec
  metric_name: user_unique_users_all_secure_scanners
  sisense_data:
    chart: 9982146
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10224291
    dashboard: 758607
    embed: v2
- name: Sec, Secure - Stage MAU, SMAU - Unique users who have used a Secure scanner
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The number of unique users who have run one or more Secure scanners.
  target: 10% higher than current month (SaaS and self-managed combined)
  org: Sec Section
  section: sec
  stage: secure
  public: true
  pi_type: Section MAU, SMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - currently instrumented to meet OKRs (see instrumentation, Lessons Learned for more details)
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free-to-OSS/education Gold/Ultimate users and paid Gold/Ultimate users
    - Threat Insights data is not yet included in this count until we support user-level metrics in Snowplow for self-managed
  lessons:
    learned:
    - Current Self-Managed Uplift is leading to massive underreporting as it doesn't fully account for non-reporting instances (offline environments, security-conscious enterprises)
  monthly_focus:
    goals:
      - Overall growth was reported as flat. This is in opposition in our Ultimate growth during this period. As such, this will need to be investigated to confirm if there was data loss.
      - Identify a way to denote underreporting on Sec-related *MAU charts to remove confusion / clarify metrics interpretation
      - Work with the Data Analytics team on improving Self-Managed Uplift for Sec
  metric_name: user_unique_users_all_secure_scanners
  sisense_data:
    chart: 9982579
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 9984584
    dashboard: 758607
    embed: v2

- name: Secure:Static Analysis - GMAU - Users running Static Analysis jobs
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The highest of the number of unique users who have run SAST or Secret Detection jobs.
  target: Progressively increasing month-over-month, >10%
  org: Sec Section
  section: sec
  stage: secure
  group: static_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track, continued healthy growth of static analysis usage. 
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free OSS Ultimate users and paid Ultimate users.
  lessons:
    learned:
      - Tracking upgrades is really difficult to today, but we're [starting to get directional data](https://app.periscopedata.com/app/gitlab/718481/Static-Analysis-Metrics---@tmccaslin?widget=11735956&udv=1090066) to better understand how many and what vectors upgrades from SAST come from. I believe that our in-product ugprade paths are now starting to see conversion. Additional ideas are being pursued to continue this trend.
  monthly_focus:
    goals:
      - Moving forward we are focused strongly on data accuracy and reduction of false positives. We are actively rolling out a [new vulnerability tracking improvement from Vulnerability Research](https://gitlab.com/groups/gitlab-org/-/epics/5144) which should materially reduce false positives. This approach is appearing to work out and produce more accurate tracking. This also opens a new metrics problem that we need a way to track accuracy with real-world data. We intend to measure the improvements of this change via the [new vulnerabilities data](https://app.periscopedata.com/app/gitlab/801442/Taylors-Vuln-Exploration) in Sisence to ensure this change results in material positive impact before rolling it out. This feature has gotten heavily delayed due to rapid action DB work and upstream bugs with vulnerability reports that our group does not directly control. 
      - We're working to [improve benchmarking capabilities](https://gitlab.com/gitlab-org/gitlab/-/issues/333054) to be better equipped to compare security results between versions of our analyzers and other third party tools to help customers and prospects answer questions about the depth and breadth of what types of vulns can we detect. 
      - To increase efficiency and reduce maintenance costs of our 10 linter analyzers we are [transitioning to SemGrep for many of our linter analyzers](https://gitlab.com/groups/gitlab-org/-/epics/5245). If successful we will reduce ~10 open source analyzers into 1 while also improving the detection rules and proving a more consistent rule management experience across these analyzers, this will be materially impactful as we seek to build our proprietary SAST engine on top of these results. [In 13.12 we released this capability](https://about.gitlab.com/releases/2021/05/22/gitlab-13-12-released/#semgrep-sast-analyzer-for-javascript-typescript-and-python) for JS, TS, and Python and are pursuring additional language transitions via [our GSoC project](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/gitlab-gsoc-2021/-/issues/3). We're already pushing 170k Semgrep jobs with ~8k more per week! 
  metric_name: xMAU, static analysis - paid GMAU and all GMAU
  sisense_data:
    chart: 9980130
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 9967764
    dashboard: 758607
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670

- name: Secure:Static Analysis - Job Growth
  base_path: "/handbook/product/performance-indicators/"
  definition: Static analysis job growth is an incicator of expanded deployment of static analysis across customer repositories. This growth is distinct from GMAU which will not increase as customers expand their rollout of secure across projects. 
  target: Progressively increasing month-over-month, >10%
  org: Product
  section: sec
  stage: secure
  group: static_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: false
  is_key: false
  health:
    level: 3
    reasons:
    - On track, continued healthy growth of static analysis usage. 
  implementation:
    status: Complete
  lessons:
    learned:
      - UI enablment features like the configuration page directly drive adoption of secure jobs as they make it easy for non-technical users to enable features with recommended configurations. We've seen amazing growth of Secret Detection largely attributed to the [13.12 addition of a coniguration button for all users](https://about.gitlab.com/releases/2021/05/22/gitlab-13-12-released/#configuration-tool-for-secret-detection).
      - Performance and failures is a key indicator of SAST analyzer health. The [new job performance and status dashboard](https://app.periscopedata.com/app/gitlab/833722/Static-Analysis-Analyzer-job-performance ) provides us an unprecedented understanding of average performance and failure trends for each individual analyzer. We also are seeing [continued improvements in successful job rates](https://app.periscopedata.com/app/gitlab/718481/Static-Analysis-Metrics---@tmccaslin?widget=10143599&udv=1090066). 
  monthly_focus:
    goals:
      - Explore upgrade paths for non-ultimate customers discovering the value of ultimate and upgrading. We're [working with the verify team](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/360) and [growth team](https://gitlab.com/gitlab-org/gitlab/-/issues/333179) to explore ways to encourage SAST usage and hopefully better track upgrades.
  metric_name: user_sast_jobs, user_secret_detection_jobs
  sisense_data:
    chart: 9381795
    dashboard: 718481
    embed: v2
  sisense_data_secondary:
    chart: 9430191
    dashboard: 718481
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670

- name: Secure:Static Analysis - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 2
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-static_analysis/stage-groups-group-dashboard-secure-static-analysis)
  implementation:
    status: Instrumentation
  lessons:
    learned:
      - This metric was created without input from the team and thus does not accuratley categorize our endpoints. [We are working to recategorize these](https://gitlab.com/groups/gitlab-org/-/epics/5992) for the entire section. 
  monthly_focus:
    goals:
      - We're actively working to [recategorize endpoints to ensure this metric is correct](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/63545). We currently are not aware of any unaddressed increased error rates related to our features. Much work has been done to address existing performance concerns with our endpoints and that work is now wrapping up.
  sisense_data:
    chart: 12191181
    dashboard: 892459
    embed: v2
  sisense_data_secondary:
    chart: 12191183
    dashboard: 892459
    embed: v2

- name: Secure:Dynamic Analysis - GMAU - Users running a Dynamic Analysis scan
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: Number of unique users who have run one or more DAST or Fuzzing jobs.
  target: Progressively increasing month-over-month, >10% per-month
  org: Sec Section
  section: sec
  stage: secure
  group: dynamic_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track, usage continues to trend upwards, not including the anomalous self-managed increase last month.
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
  lessons:
    learned:
      - Self-managed usage has shrunk since last month, pointing to April's usage boost being due to trials or PoVs, not a true bump in users. A way to filter out trial/PoV users would be extremely helpful in GMAU metrics.
      - Fuzzing is not currently included in the Dynamic Analysis charts, so there is not a good single source of truth for Dynamic Analysis GMAU.
  monthly_focus:
    goals:
      - Finishing [noisy vulnerabilities aggregation](https://gitlab.com/gitlab-org/gitlab/-/issues/254043) to stop DAST from overwhelming the Vulnerability Report.
      - Adding [browser-based authentication to replace the current authentication process](https://gitlab.com/gitlab-org/gitlab/-/issues/331751). This provides better troubleshooting when authentication fails and addresses a major pain point for DAST users.
      - DAST API scans switching to use the [API Security engine](https://gitlab.com/groups/gitlab-org/-/epics/4254).
  metric_name: user_dast_jobs
  sisense_data:
    chart: 9980137
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 9980139
    dashboard: 758607
    embed: v2
  sisense_data_tertiary:
    chart: 9926832
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670

- name: Secure:Dynamic Analysis - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-dynamic_analysis/stage-groups-group-dashboard-secure-dynamic-analysis)
  sisense_data:
    chart: 12191204
    dashboard: 892459
    embed: v2
  sisense_data_secondary:
    chart: 12196879
    dashboard: 892459
    embed: v2

- name: Secure:Composition Analysis - GMAU - Users running any SCA scanners
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more Dependency Scanning jobs, or number of unique users who have run one or more License Scanning jobs.
  target: 2% higher than current month (SaaS and self-managed combined) for All and Paid
  org: Sec Section
  section: sec
  stage: secure
  group: composition_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  implementation:
    status: Complete
    reasons:
    - Currently our data is a lagging indicator (the majority of our users are self hosted and update 3 months behind current), trends are accurate but due to extrapolation of self-hosted, low opt-in of self-hosted, overall missing granularity, and 28 day cycles actual numbers are imprecise.
  lessons:
    learned:
      - Nothing through use of xMAU Metrics.
  monthly_focus:
    goals:
      - 13.11 and 13.12 team was dedicated to rapid action, we still have a few lingering issues and are also working on [Software Composition Analysis removals and deprecations for 14.0](https://about.gitlab.com/blog/2021/02/08/composition-analysis-14-deprecations-and-removals/) and the delayed bugs from 13.11+. 14.0-14.1 we are focusing on improving testing to speed up and make easier development of features as well as make it easier for community contriutors to test. [Auto remediation](https://gitlab.com/groups/gitlab-org/-/epics/3188) is still blocked by a token issue.
  metric_name: user_license_management_jobs, user_dependency_scanning_jobs
  sisense_data:
    chart: 9967897
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10102606
    dashboard: 758607
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670

- name: Secure:Composition Analysis - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - The majority of our budget is from parsing of our artifacts (which grow with the size and complexity of the project) which tend to be over 1 second, which is the default value for error budgets. We will be reviewing our configuration and settings to see if we can gather any performance improvements however we also plan to seporate out those specific items and enable them to have a longer error budget if that is within a range acceptable to users.
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-composition_analysis/stage-groups-group-dashboard-secure-composition-analysis)
  sisense_data:
    chart: 12196882
    dashboard: 892459
    embed: v2
  sisense_data_secondary:
    chart: 12196885
    dashboard: 892459
    embed: v2

- name: Secure:Threat Insights - GMAU - Users interacting with Secure UI
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: Number of unique sessions viewing a security dashboard, pipeline security
    report, or expanding MR security report. Same as Paid until OSS projects
    can be separated.
  target: Progressively increasing month-over-month, >2% per-month
  org: Sec Section
  section: sec
  stage: secure
  group: threat_insights
  public: true
  pi_type: GMAU
  product_analytics_type: SaaS
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - GMAU dipped 2.5%. Unsure if this is still a data integrity issue. Total (non-unique) page views are also down about 7%.
  implementation:
    status: Definition
    reasons:
    - Collection is complete for .com only
    - Snowplow data is session only, not unique users
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
    - Cannot separate out GitLab employee usage from customers
  lessons:
    learned:
      - Unclear if April and May drops are data issues or a legitimate signal. Most significant drop in page views in a year.
      - Even if data is incomplete, growth did occur MoM for most areas outside Security Center. Drop may be attributable entirely to decreased usage here.
  monthly_focus:
    goals:
      - Begin to adjust roadmap based on Kano survey results
      - Continue focus on key technical debt around performance, scalability, and maintainability
      - Evaluate main drivers of [Error Budget overages](https://about.gitlab.com/handbook/product/sec-section-performance-indicators/#securethreat-insights---error-budget-for-gitlabcom) and work to address
  metric_name: n/a
  sisense_data:
    chart: 9244137
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670

- name: Secure:Threat Insights - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-threat_insights/stage-groups-group-dashboard-secure-threat-insights)
    - RCA for [Error budget and remediation](https://gitlab.com/gitlab-org/gitlab/-/issues/329414)
  sisense_data:
    chart: 12196887
    dashboard: 892459
    embed: v2
  sisense_data_secondary:
    chart: 12196889
    dashboard: 892459
    embed: v2

- name: Protect:Container Security - SMAU, GMAU - Users running Container Scanning or interacting with Protect UI
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The number of users who have run a container scanning job or interacted with the Protect UI in the last 28 days of the month.
  target: Progressively increasing month-over-month, >2%
  org: Sec Section
  section: sec
  stage: Protect
  group: container_security
  public: true
  pi_type: SMAU, GMAU
  product_analytics_type: Both
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Usage growth dropped slightly from its April high. We may continue to see a small, temporary decline in usage as customers adapt their pipelines to work with the new Trivy scanner.  Long-term, this is expected to provide a significant boost to usage.
    - Although MAU data of the Alert Dashboard is not yet included in the broader numbers for these charts, we are seeing positive early signs from the data we do have that usage is increasing sharply.
  implementation:
    status: Complete
    reasons:
    - Limitations in data (see SCA limitations above)
    - Although we have released the Alert Dashboard, we do not yet have a good way of tracking monthly active users for this area. Instrumentation is expected to be complex (1-2 months of one developer's time) and usage is expected to remain low until additional alert times are added to the dashboard. Currently instrumenting these metrics is a low priority.
  lessons:
    learned:
      - The switch from Clair to Trivy has allowed us to close 20+ outstanding customer issues and bugs.  Adoption is expected to increase significantly after the 14.0 release.
  monthly_focus:
    goals:
      - Replacing Clair with Trivy (https://gitlab.com/groups/gitlab-org/-/epics/5398) will be the default beginning in the 14.0 release and will set the foundation for us to be able to scan containers in production environments
      - Allow Users to Edit yaml-mode Scan Execution Policies in the Policy UI (https://gitlab.com/groups/gitlab-org/-/epics/5362) is being worked on for a July release.  This change will allow us to default the policy feature flag to on.
  metric_name: user_container_scanning_jobs
  sisense_data:
    chart: 10039269
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039569
    dashboard: 758607
    embed: v2

- name: Protect:Container Security - SMAC, GMAC - Clusters using Container Network or Host Security
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The number of active clusters with at least one Container Security feature
    enabled (Network Policies, or Host Security) in the last 28 days.
  target: Progressively increasing >10% month-over-month
  org: Sec Section
  section: sec
  stage: Protect
  group: container_security
  public: true
  pi_type: SMAC, GMAC
  product_analytics_type: Both
  plan_type: Both
  is_primary: false
  is_key: false
  health:
    level: 3
    reasons:
    - Growth from April -> May was fairly stagnant
    - The installation of Cilium has been failing in modern versions of Kubernetes, likely explaining the stagnation in new users.  We do not currently have a SET for this area and did not have tests to cover this.  We are [working to fix](https://gitlab.com/gitlab-org/gitlab/-/issues/330131) this critical bug.
    - We are not actively investing in this area right now, so new feature development has stopped
  implementation:
    status: Complete
    reasons:
    - Data collection for CHS is [planned but not yet implemented](https://gitlab.com/gitlab-org/gitlab/-/issues/218800) as the category has been minimally resourced since it was released.
  lessons:
    learned:
      - We have 26 clusters using CNS, 23 of which are on gitlab.com and 3 are self managed.  Other self-managed customers may be using CNS but not reporting usage ping.
      - Customers are using the blocking mode as a significant amount of traffic is being dropped.
      - With the Defend -> Protect changes, we have significantly reduced our investment in this area
  monthly_focus:
    goals:
      - We are temporarily not investing in this area so we can allocate more resources to Security Orchestration and Container Security
  metric_name: clusters_applications_cilium
  sisense_data:
    chart: 9620410
    dashboard: 694854
    embed: v2

- name: Protect:Container Security  - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-container_security/stage-groups-group-dashboard-protect-container-security)
  sisense_data:
    chart: 12196932
    dashboard: 892737
    embed: v2
  sisense_data_secondary:
    chart: 12196940
    dashboard: 892737
    embed: v2
    
