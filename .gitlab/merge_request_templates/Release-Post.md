Process Improvements? Have suggestions for improving the release post process as we go?
Capture them in the [Retrospective issue](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/TODO).

- **Preview page** (shows latest merged content blocks for reference till the 17th): https://about.gitlab.com/releases/gitlab-com/
- **View App** (shows introduction, MVP and latest merged content blocks for reference 18th - 21st: https://release-X-Y.about.gitlab-review.app/releases/YYYY/MM/DD/gitlab-X-Y-released/index.html


_Release post:_

- **Intro**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/sites/uncategorized/source/releases/posts/YYYY-MM-22-gitlab-X-Y-released.html.md
- **Items**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/release_posts/X_Y
- **Images**: https://gitlab.com/gitlab-com/www-gitlab-com/tree/release-X-Y/source/images/X_Y

_Related MRs:_
- **Bug Fixes:** Link MR for bug fixes here
- **Usability improvements:** Link MR for usability improvements here
- **Performance improvements:** Link MR for performance improvements here

_Related files:_

- **Features YAML** link: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/features.yml
- **Features YAML Images** link: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/source/images/feature_page/screenshots
- **Homepage card**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/sites/uncategorized/source/includes/home/ten-oh-announcement.html.haml
- **MVPs**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/mvps.yml

_Release post branch ownership:_
- The **Release Post Manager** is solely in charge of changes to the release post branch. To avoid potential merge conflicts later during content assembly, it is imperative that TWs and Messaging leads do not merge updates from `master` to the release post branch even if it is falling behind. The Release Post Manager will take care of merging updates from master as part of the content assembly process on the 18th and work with the Technical Advisor as needed.

_Handbook references:_

- Blog handbook: https://about.gitlab.com/handbook/marketing/blog/
- Release post handbook: https://about.gitlab.com/handbook/marketing/blog/release-posts/
- Markdown guide: https://about.gitlab.com/handbook/engineering/technical-writing/markdown-guide/

_People:_

- Release Post Managers: https://about.gitlab.com/handbook/marketing/blog/release-posts/managers/
- Release Managers: https://about.gitlab.com/community/release-managers/

| Release post manager | Tech writer | Technical Advisor | Messaging | Social | Product Operations DRI |
| --- | --- | --- | --- | --- | --- |
| `@release_post_manager` | `@tw_lead` | `@tech_advisor` | `@pmm_lead` | DRI: `@wspillane` & `@social` for Slack Checklist item | `@fseifoddini` |

### Release post kickoff (`@release_post_manager`)

**Due date: YYYY-MM-DD** (By the 7th)

**Before starting on this checklist, you should have created the release post branch and required files [as explained in the Handbook](https://about.gitlab.com/handbook/marketing/blog/release-posts/#create-your-release-post-branch-and-required-directoriesfiles)**

**Note:** Throughout the release post process, you'll do various Slack reminders/announcements. It is recommended you cc the Product Operations DRI and the rest of your release post team as you do these Slack posts because it helps keep everyone on the same page.

- [ ] Prior to your first team standup consider setting up a coffee chat with the previous release post manager and/or Product Operations to ask for tips and any helpful "latest info"
- [ ] After meeting with the previous release post manager and/or Product Operations for insights, consider setting up a meeting with your release post shadow to help them understand their role and how much capacity they have to support the work that month
- [ ] Verify this MR is labeled ~"blog post" ~release ~"release post" ~"priority::1" and assigned to you (the Release Post Manager)
- [ ] Add the current milestone to this MR
- [ ] Create a release post retrospective issue by using the [Release post retrospective template](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/.gitlab/issue_templates/Release-Post-Retrospective.md), and use `Release Post X.Y Retrospective` as a title. Once created, update the link at the top of this MR description. [Example retro issue](hhttps://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11011).
    - [ ] Schedule a 50 min Live Retrospective meeting for some time after the 22nd. All action items for the retro need to be completed prior to the 1st of the next month in order to incorporate any process changes before the next release begins. Make sure to invite Product Ops to the Live Retro meeting. Product Ops will need to approve any major updates to the process identified during the Retrospective.
- [ ] Replace each `@mention` in this MR description with the names of the Release Post Manager, Tech Writer, Messaging Lead, and Social Lead for this release
- [ ] Update the links in this MR description
- [ ] Update all due dates in this MR description
- [ ] Make sure the release post branch has all initial files: `sites/uncategorized/source/releases/posts/YYYY-MM-DD-gitlab-X-Y-released.html.md`, `data/release_posts/X_Y/mvp.yml` and `data/release_posts/X_Y/cta.yml`
- [ ] Add the release number and your name as the author to `sites/uncategorized/source/releases/posts/YYYY-MM-22-gitlab-X-Y-release.html.md`
- [ ] Per guidance on [communication](https://about.gitlab.com/handbook/marketing/blog/release-posts/#communication) for the release, create the `X-Y-release-post-prep` channel in Slack, and invite `@tw_lead`, `@pmm_lead`, `@tech_advisor`, Product Operations_DRI and the release post manager shadow (whoever is running the next release post. Let them know this channel is to discuss production specific topics that don't concern the broader team, so we can keep #release-post focused and easy to follow. As a topic, add the release post MR, the link to the review app, and the link of the retro issue:

  ```
  MR: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/1234
  Review App: https://release-X-Y.about.gitlab-review.app/releases/YYYY/MM/DD/gitlab-X-Y-released/
  Retro issue: https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/1234
  ```
- [ ] Update the topic in the #release-post channel:

  ```
  MR: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/1234
  Preview page: https://about.gitlab.com/releases/gitlab-com/
  Review App: https://release-X-Y.about.gitlab-review.app/releases/YYYY/MM/DD/gitlab-X-Y-released/
  ```
- [ ] Announce yourself as the Release Post Manager in the #release-post channel, naming the Messaging lead, Technical Advisor, and TW lead as well for reference. cc Product Operations DRI
- [ ] Set up a 15-minute weekly standup with the TW lead, Tech advisor and Messaging lead to touch base and troubleshoot. Invite the Product Operations DRI as well, as they will be your backup on all tasks should complications arise. If times zones conflict, this is not mandatory. Sample [agenda](https://docs.google.com/document/d/1LhAEBgZmSq8gs7Dm60RwqU60EZK5cafvILgQUoiakBM/edit#)
- [ ] In the #release-post Slack channel, remind Product Managers that all [content blocks (features, recurring, bugs, etc.)](#content-blocks) should be drafted, and under review by the 10th. All direction items and notable community contributions should be included in the release post.
- [ ] Confirm your local dev environment is running a current version of Ruby. See Handbook section [Local dev environment setup to run content assembly script](https://about.gitlab.com/handbook/marketing/blog/release-posts/#local-dev-environment-setup-to-run-content-assembly-script).
- [ ] Remind TW and Messaging lead (either via slack or weekly standup) not to merge in changes from `master` to the release post branch. See the section `Release post branch ownership` above for more details.

### Release post item creation reminders (`@release_post_manager`)

**Note:** Throughout the release post-process, you'll do various Slack reminders/announcements. It is recommended you cc the Product Operations DRI and the rest of your release post team as you do these Slack posts because it helps keep everyone on the same page.

**Due date: YYYY-MM-DD** (By the 10th)
- [ ] Remind the product managers in the #release-post channel that today is the day to have Release Post Items created and in review by the PMM and TW counterparts.
- [ ] Remind the product managers in the #release-post channel that it's important we all follow the [file naming guidelines](https://about.gitlab.com/handbook/marketing/blog/release-posts/#instructions) as it affects the release post template as well as the release post teams ability to find release post items with ease.
- [ ] Create the [bugs, usability improvements and performance improvements MRs](https://about.gitlab.com/handbook/marketing/blog/release-posts/#create-mrs-for-usability-improvements-bugs-and-performance-improvements)
    - [ ] Link these MRs to the top section of this MR so that they are easy to access
    - [ ] Inform EMs the bugs and performance improvement MRs are ready to receive their contributions by mentioning `@gitlab-com/backend-managers` and `@gitlab-org/frontend/frontend-managers`  in the comments of this MR, asking them to add their notable team performance improvements and bugs fixes to the MRs you've created by providing them with the direct links
    - [ ] Copy the link from the above comment in this MR to `@gitlab-com/backend-managers` and `@gitlab-org/frontend/frontend-managers` and share it in  Slack #development and Slack #eng-managers as well
    - [ ] In Slack #release-post, share the link to just the Bug Fixes MR and ask PMs to work with their EM to add high impact bugs to the MR
    - [ ] In Slack #release-post, share a link to just the Usability Improvements MR and ask PMs to work in partnership with their Product Designers to add line items that highlight significant usability improvements.
        - Remind them that we have to limit to content block to [notable improvements](https://about.gitlab.com/handbook/marketing/blog/release-posts/#contributing-to-usability-improvements) but they can add as many items as they'd like to the [UI Polish gallery](https://nicolasdular.gitlab.io/gitlab-polish-gallery/)

### Recurring items starting on the 12th: `@release_post_manager`

**General Content Review**
As PMs finalize their release post items it can be helpful for the RPM to review and offer feedback. This reduces pressure on the 17th as items are merged and provides additional review from someone with a fresh perspective. You can start this as early as the 12th, but this should be an ongoing task leading up to content assembly on the 18th. Review each MR labeled ~Ready for content that follows handbook guidance. See [What RPM should look for when reviewing content blocks](https://about.gitlab.com/handbook/marketing/blog/release-posts/#what-rpm-should-look-for-when-reviewing-content-blocks).

To easily manage and track reviewed items do the following:

- [ ] Bookmark a filtered MR list similar to [this](https://gitlab.com/dashboard/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=release%20post%20item&not[label_name][]=rp%20manager%20reviewed&label_name[]=release%20post&label_name[]=Ready) to track release post items you haven't reviewed. (note: add the correct milestone to that filter)
- [ ] Add the ~"rp manager reviewed" label to any RP item you've reviewed.


**Reminding and Alerting DRIs**

It's important to keep DRIs up to date regularly with items they need to deliver for the release post. Especially given how async and distributed GitLab team members are early reminders are very helpful.

- [ ] Alert DRIs (PMs, EMs and others as needed) at least one working day before each due date (post a comment to #release-post Slack channel)

### General contributions `@release_post_manager`

The release post is authored following a changelog-style system.
Each item should be in an individual YAML file.

#### Contribution instructions

See [Handbook: Contributing to the release post](https://about.gitlab.com/handbook/marketing/blog/release-posts/#general-contributions).

#### Content blocks

**Due date: YYYY-MM-DD** (10th)

Product Managers are responsible for [raising MRs for their content blocks](https://about.gitlab.com/handbook/marketing/blog/release-posts/#pm-contributors) and ensuring they are reviewed by necessary contributors by the due date. Content blocks should also be added for any epics or notable community contributions that were delivered.

Product Managers are also responsible for making sure all [required (Tech Writing) and recommended (PM Director and PMM) reviews ](https://about.gitlab.com/handbook/marketing/blog/release-posts/#reviews) get done for their content blocks. To help reviewers prioritize what to review, PMs should communicate which content blocks are most important for review by applying the proper labels to the release post item MR prior to assigning the MR to reviewers. (ex: Tech Writing, Direction, Deliverable, etc). PMs can also [follow these guidelines](https://about.gitlab.com/handbook/marketing/blog/release-posts/#recommendations-for-optional-director-and-pmm-reviews) to help decide which content blocks should get PM Director and PMM reviews.

To enable Engineering Managers [to merge the content blocks](https://about.gitlab.com/handbook/marketing/blog/release-posts/#merging-content-block-mrs) as soon as an issue has closed, PMs should ensure all scheduled items have MRs created for them and have the Ready label applied when content contribution and reviews are completed.

Product Managers should only check their box below when **all** their content blocks (features, deprecations, etc.) are complete (documentation links, images, etc.). Please don't check the box if there are still things missing.

_Reminder: be sure to reference your Direction items and Release features._ All items which appear
in our [Upcoming Releases page](https://about.gitlab.com/upcoming-releases/) should be included in the relevant release post.
For more guidance about what to include in the release post please reference the [Product Handbook](https://about.gitlab.com/handbook/product/product-processes/#communication#release-posts).

- Manage
  - [ ] Access (`@ogolowinski`)
  - [ ] Compliance (`@stkerr`)
  - [ ] Import (`@hdelalic`)
  - [ ] Optimize (`@ljlane`)
- Create
  - [ ] Source Code (`@sarahwaldner`)
  - [ ] Code Review (`@phikai`)
  - [ ] Editor (`@ericschurter`)
  - [ ] Gitaly (`@mjwood`)
  - [ ] Ecosystem (`@deuley`)
- Plan
  - [ ] Project Management (`@gweaver`)
  - [ ] Product Planning (`@cdybenko`)
  - [ ] Certify (`@mjwood`)
- Verify
  - [ ] Pipeline Execution (`@jreporter`)
  - [ ] Pipeline Authoring (`@dhershkovitch`)
  - [ ] Runner (`@DarrenEastman`)
  - [ ] Testing (`@jheimbuck_gl`)
- Package
  - [ ] Package (`@trizzi`)
- Release
  - [ ] Release (`@kbychu`)
- Configure
  - [ ] Configure (`@nagyv-gitlab`)
- Monitor
  - [ ] Monitor (`@kbychu` `@crystalpoole`)
- Secure
  - [ ] Static Analysis (`@tmccaslin`)
  - [ ] Dynamic Analysis (`@derekferguson`)
  - [ ] Composition Analysis (`@NicoleSchwartz`)
  - [ ] Threat Insights (`@matt_wilson`)
  - [ ] Vulnerability Research (`@tmccaslin`)
- Protect
  - [ ] Container Security (`@sam.white`)
- ModelOps
  - [ ] Applied Machine Learning (`@tmccaslin`)
- Growth
  - [ ] Activation (`@jstava`)
  - [ ] Conversion (`@s_awezec`)
  - [ ] Expansion (`@s_awezec`)
  - [ ] Adoption (`@mkarampalas`)
  - [ ] Product Intelligence (`@amandarueda`)
- Fulfillment
  - [ ] Purchase (`@tgolubeva`)
  - [ ] License (`@teresatison`)
  - [ ] Utilization (`@amandarueda`)
- Enablement
  - [ ] Distribution (`@dorrino`)
  - [ ] Geo (`@nhxnguyen`)
  - [ ] Memory (`@fzimmer`)
  - [ ] Global Search (`@JohnMcGuire`)
  - [ ] Database (`@fzimmer`)
  - [ ] Sharding (`@fzimmer`)
  - [ ] Infrastructure (`@awthomas`)
- Mobile Apps (`@david`)
- Five Minute Production App (`@kencjohnston`)

#### Recurring content blocks

**Due date: YYYY-MM-DD** (10th)

The following sections are always present and managed by the PM or Eng lead
owning the related area.

- [ ] Add GitLab Runner improvements: `@DarrenEastman`
- [ ] Add Omnibus improvements: `@dorrino`
- [ ] Add Mattermost update to the Omnibus improvements section: `@dorrino`

**Due date: YYYY-MM-DD** (15th)

- [ ] [Add Performance Improvements](https://about.gitlab.com/handbook/marketing/blog/release-posts/#create-mrs-for-usability-improvements-bugs-and-performance-improvements) section: `@release post manager`
- [ ] [Add Bug Fixes](https://about.gitlab.com/handbook/marketing/blog/release-posts/#create-mrs-for-usability-improvements-bugs-and-performance-improvements) section: `@release post manager`
- [ ] [Add Usablity ](https://about.gitlab.com/handbook/marketing/blog/release-posts/#create-mrs-for-usability-improvements-bugs-and-performance-improvements) section: `@release post manager`

#### Final Merge

**Due date: YYYY-MM-DD** (17th)

Engineering managers listed in the MRs are responsible for merging as soon as the implementing issue(s) are officially part of the release. All release post items must be merged on or before the 17th of the month. Earlier merges are preferred whenever possible. If a feature is not ready and won't be included in the release, the EM should push the release post item to the next milestone.

To assist managers in determining whether a release contains a feature. The following procedure [documented here](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/34519) is encouraged to be followed. In the coming releases, Product Management and Development will prioritize automating this process both so it's less error-prone and to make the notes more accurate to release cut.

### Content assembly and initial review (`@release_post_manager`)

Note: `Final Content Assembly`, `Messaging Lead Review`, and `Structural Check` steps all happen in sequence on the 18th starting ~8am PST (`America/Los_Angeles`). If the Release Post Manager, TW Lead, and Messaging Lead span many timezones it's recommended you coordinate ahead of the 18th to understand how this could impact working hours for each team member. If need be, the time of initiating the `final content assembly` and the subsequent coordinated tasks can be shifted, as long as `Final content review` with the CEO and CProdO begin no later ~12pm PST on the 19th, to allow enough time for feedback/updates.


**Due date: YYYY-MM-DD** (12th)

- [ ] Per the instructions in the handbook request [MVP nominations](https://about.gitlab.com/handbook/marketing/blog/release-posts/#mvp) with a link to an issue for collaboration. **Be sure to follow the instructions in the handbook page to maximize contributions to the MVP issue.**
- [ ] Remind PMs/EMs to contribute to the bugs, usability, and performance improvement MRs by commenting on the various Slack threads you initiated in #release-post, #development and  #eng-managers by the 10th

**Due date: YYYY-MM-DD** (15th)

- [ ] Select a [cover image](https://about.gitlab.com/handbook/marketing/blog/release-posts#cover-image) for the release post
- [ ] Verify that the selected cover image has not been used before.
  - Tip: MacOS users, navigate to the `source/images/` directory and use the search bar in the Finder to search for `cover`. Make sure the scope is set to only search "images". This won't reveal all previous images, but the last couple of years have had pretty consistent naming.
- [ ] On the `release-X-Y` branch, add the cover image to `source/images/X_Y/X_Y-cover-image.jpg`. Tip: Be sure to use an `_` between release numbers, not a `-`
- [ ] On the `release-X-Y` branch, in `sites/uncategorized/source/releases/posts/YYYY-MM-DD-gitlab-X-Y-released.html.md`, [add details about the source image](https://about.gitlab.com/handbook/marketing/blog/release-posts/#cover-image-license).
- [ ] Choose an [MVP](https://about.gitlab.com/handbook/marketing/blog/release-posts/#mvp) for this release based on what's surfaced in the MVP issue
  - [ ] If no MVP nominations have been added to the MVP issue by the 15th, send reminders in Slack with the link to the MVP issue. An easy way to do this is to respond to your original Slack solicitation posts and resend to the whole channel.
  - [ ] Once one or more quality nominations have been received, choose one and notify via Slack #release-post of your choice. Use this chance to solicit any last-minute nominations and confirm that the contribution your pick was nominated for will make it into this release.
- [ ] Before handing off the bugs, usability and performance improvements MRs to the TW lead for final review, remind PMs/EMs about the content due date by revisiting and commenting on the Slack threads you created by the 10th in #release-post #development and #eng-managers. Let them know it's "last call" and no further contributions to the MRs will be taken after the 15th.```
- [ ] In the threads of this MR, look for a [GitLab Mattermost update](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/78893#note_546017926). Please tag the [Distribution PM](https://about.gitlab.com/handbook/product/categories/#distribution-group) as FYI and ask them to resolve the thread.

**Due date: YYYY-MM-DD** (17th)

- [ ] Mention the [Distribution PM](https://about.gitlab.com/handbook/product/categories/#distribution-group) in Slack #release-post, reminding them to add any relevant [upgrade warning](https://about.gitlab.com/handbook/marketing/blog/release-posts/#important-notes-on-upgrading) by doing an [upgrade MR](https://about.gitlab.com/handbook/marketing/blog/release-posts/#upgrades)
- [ ] Ask the [Messaging Lead](https://about.gitlab.com/handbook/marketing/blog/release-posts/#messaging-lead) in Slack #release-post if they're on track to finalize the introduction by the 18th or if they need help pinning down the final themes/features for the intro
- [ ] If there are no deprecation MRs, ask in Slack #release-post if there are any deprecations to be included yet
- [ ] Finalize your [MVP](https://about.gitlab.com/handbook/marketing/blog/release-posts/#mvp) selection and work with the nominator of the MVP to write the MVP section in `data/release_posts/X_Y/mvp.yml` on the `release-X-Y` branch
- [ ] On the `release-X-Y` branch, add the MVP's name and other profile info to `data/mvps.yml`
- [ ] In Slack #release-post remind all PMs that it's the 17th so they need to either have their EMs merge their release post item MRs or bump the milestone if they know it won't make it

**Due date: YYYY-MM-DD** (18th at 8 AM PT and NO earlier)

- [ ] Perform **final content assembly** by pulling all content block MRs merged to master into the release post branch by using the following commands locally (one command at a time):
  ```
  git checkout master
  git pull
  git checkout release-x-y
  git pull
  git merge master
  bin/release-post-assemble
  git push origin release-x-y
  ```
- [ ] Do a visual check of the release-X-Y content block and image folders to make sure paths and names are correct
- [ ] Make sure the release post branch View App generates as expected
- [ ] Do a visual check of the blog post and ordering of content blocks for secondary items to confirm they are grouped by stage in descending alphabetical order.

- [ ] Notify the PM team in #release-post Slack channel that final content assembly has happened and all work must now shift from master onto the release post branch via coordination with the release post manager.
     - Include a link to the `View App`, asking them to make sure all their content is showing up as expected with correct image/video links, etc. and that as confirmation of their final review, to check off their [content and recurring blocks](#content-blocks) in the release post MR.
     - Be sure to link them to the [content and recurring blocks](#content-blocks) section in the MR as part of the post

**Note**: If the release post assembly script fails, look at the bottom of [this section](https://about.gitlab.com/handbook/marketing/blog/release-posts/#merge-individual-items-in-to-your-branch) of the release post handbook page for further instruction

**Due date: YYYY-MM-DD** (18th)
- [ ] Label this MR: ~"blog post" ~release ~"review-in-progress"
- [ ] Check if there are no broken links in the View App (use a dead link checker, e.g., [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf))
  - [ ] Links to confidential issues may be missed. It is helpful to check for broken links as an unauthenticated GitLab user (either logged out, in another browser, or in Incognito mode).
  - [ ] If there are links to external blogs that are still broken in the View app, check with PMs, Messaging leads and others as needed to make sure the referenced blogs go live before the 22nd
- [ ] Check all comments in the MR thread (make sure no contribution was left behind)
- [ ] Make sure all discussions in the thread are resolved
- [ ] Assign the MR to the next reviewer (messaging lead)

### Other reviews

Ideally, complete the reviews by the 19th of the month, so that the 2 days before the release can be left for fixes and small improvements.

#### [Marketing content and positioning](https://about.gitlab.com/handbook/marketing/blog/release-posts/#messaging-lead) (Messaging Lead)

The Marketing content, positioning, and reviews are done by the Messaging lead: `@pmm_lead`

Use the process listed in the [release post handbook page](https://about.gitlab.com/handbook/marketing/blog/release-posts/#messaging-lead). The checklist below outlines *what* to do and *when* to do it, while the handbook goes into more detail on *how* to perform each task.

**Due date: YYYY-MM-DD** (13th)

- [ ] Create a shortlist of top release themes/features.
- [ ] Post a message on #release-post slack channel and ping `@product-team` `@pmm-team` for feedback on top features (give a list of 4-5 themes or 8-10 features).

**Due date: YYYY-MM-DD** (15th)

- [ ] Consolidate the feedback you received from the PM and PMMs teams, update your themes, and ask for final theme review from the Chief Product Officer `@sfwgitlab` and the VP of Product `@adawar` in the `#release-post` slack channel using the template defined in https://about.gitlab.com/handbook/marketing/blog/release-posts/#template-for-reviews.

**Due date: YYYY-MM-DD** (16th)

- [ ] Commit the first (rough) draft of the release post introduction with the various themes/features selected above in YYYY-MM-DD-gitlab-X-Y-released.html.md in the release X-Y branch. Notify the TW lead and release post manager, along with the PMs & PMMs whose features are listed in the intro for review.
- [ ] Ping the Marketing Events & Field teams to see if there is an upcoming event to promote.
    - [ ] update `data/release_posts/X_Y/cta.yml` with any relevant CTA (Commit promotion, for example). Ensure that `X_Y` in the file path reflects the current release. If there isn't a specific call to action to add, use this as the default button text: "Join us for an upcoming event" with a link to '/events/'. Reference [this example from 13.1](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/52057/diffs#diff-content-fc2520a82e843973f9c87fded973c2555a4f3bbf) under `data/release_posts/13_1/cta.yml` if needed.

**Due date: YYYY-MM-DD** (17th)

- [ ] Update the intro based on the feedback and polish it such that it is ready for final content review on the 18th/19th as appropriate. Notify the release post manager that you have completed revisions on the intro.

**Due date: YYYY-MM-DD** (18th)

On the 18th several tasks need to be performed after the release post manager has performed the content assembly.

- [ ] Finalize your theme and intro content based on which features actually merged and can be mentioned in the release post.
    - [ ] Link the release post times mentioned in the intro to the item blocks within the release post. For example, for a feature named "Define test cases in GitLab", the link from the introduction should point to "#define-test-cases-in-gitlab"
- [ ] [Reorder primary features](https://about.gitlab.com/handbook/marketing/blog/release-posts/#feature-order) in the `data/release_posts/XX_X` folder according to their relevance per the theme of your release post intro. Order the post by changing the file names to alphanumeric names (e.g. `01_feature_name.yml`, `02_feature_name.yml`, etc)
  - [ ] Mark the 1st item in the post as `top` while the rest will be `primary` or `secondary` based on what the [PM has decided](https://about.gitlab.com/handbook/marketing/blog/release-posts/#primary-vs-secondary).
  - [ ] If you think any features should change from `primary` to `secondary` add a suggestion to the release post item `yml` and ping the PM owner to review & apply it.
- [ ] Add a sentence in the introductory paragraph to state how many "significant new features and improvements" (top, primary, and secondary features, plus noted performance improvements) have been shipped. See [13.9 release post opening lines](https://about.gitlab.com/releases/2021/05/22/gitlab-13-12-released/) as an example. To get the number, you'll do a hand count of just features and performance improvements (do not include bugs, upgrades, etc.) in `/data/release_posts/X_Y` on the current `release-X-Y ` branch.
    - [ ] Add a video link to the kickoff call recording at the bottom of the release post introduction. You can find the video once it's available on the [direction page](https://about.gitlab.com/direction/kickoff/).
- [ ] on the `release-x-y` branch, update the release blurb for the homepage (check `/sites/uncategorized/source/includes/home/ten-oh-announcement.html.haml`). In the announcement file, update the description of the blog post with the relevant details for the current release including the current release number and dates as appropriate.
- [ ] ensure that the social sharing text for the click to tweet button on the bottom of the release post is available in the introduction

- [ ] Once all of your tasks above are complete:
    - [ ] Assign the MR back to the next reviewer (TW lead for Structural Check) and unassign yourself
    - [ ] Notify release post manager that you're done with the messaging lead tasks needed for **final content review**, by pinging them in the Slack X-Y release post prep channel

**Due date: YYYY-MM-DD** (20th)

- [ ] Did anything change between the 18th and 20th? (e.g. features slipped, additional changes based on final review, etc.) review all of the tasks due on the 18th to make sure that you update all the places that need to be updated
- [ ] Check if the number of features you added in the introductory paragraph has changed. To get the number, you'll do a hand count of just features and performance improvements (do not include bugs, upgrades, etc.) in `/data/release_posts/X_Y` on the current `release-X-Y ` branch.
- [ ] Post in the #release-post channel: _Hello PMs! The following features are top/primary!_ (Provide link to View App and tag the PMs of the top/primary features listed in the release post). _Please let us know if any of your merged primary release post items shifted out of the release after the 18th and will not make it into the final release packages by the 22nd._
     - Tell them they can [check this query](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&milestone_title=13.10&not%5Blabel_name%5D%5B%5D=released%3A%3Acandidate) (modify by the milestone) and check with their EMs to verify that it did make it.
     - It is the Messaging Lead's responsibility to make sure any top/primary items mentioned in the introduction are accurate prior to the 22nd since release post items can sometimes move in/out of the packaged release after the 18th, and this could affect the themes, headline, etc.
     - If you learn that any top/primary items have moved in/out of the packaged release after the 18th, then communicate this directly to stop or start associated actions, with the DRIs for: [Technical Marketing](https://about.gitlab.com/handbook/marketing/strategic-marketing/technical-marketing/) (the TMM team) who produce demo videos per release, [Social Marketing](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/) who produce feature campaigns per release, [Corporate Communications](https://about.gitlab.com/handbook/marketing/corporate-marketing/corporate-communications/) who lead media outreach and may have produced a press release, and any related efforts you're aware of e.g. for related blog posts.

#### [Structural check](https://about.gitlab.com/handbook/marketing/blog/release-posts/#structural-check) (Technical Writing Lead)

**Due date: YYYY-MM-DD** (18th)

The structural check is performed by the technical writing lead: `@tw_lead`

For suggestions that you are confident don't need to be reviewed, change them locally
and push a commit directly to save the PMs from unneeded reviews. For example:

- clear typos, like `this is a typpo`
- minor front matter issues, like single quotes instead of double quotes, or vice versa
- extra whitespace

For any changes to the content itself, make suggestions directly on the release post
diff, and be sure to ping the reporter for that block in the suggestion comment, so
that they can find it easily.

The TW lead is also responsible for ensuring that the [next version of the documentation site is published](https://about.gitlab.com/blog/2019/04/12/marcel-amirault-contributor-post/) to match the current release. This process should start around the same time as the release post structural check, and finish on or near the 22nd.

- [ ] Add the label ~review-structure.

In the `www-gitlab-com` repository:

- [ ] Check [frontmatter](https://about.gitlab.com/handbook/marketing/blog/release-posts/#frontmatter) entries and syntax.
- [ ] Check that the item's `name` contains backticks when referring to code. (Previously we had to use single quotes, but backticks work now.)
- [ ] Check all images (png, jpg, and gifs) are smaller than 150 KB each.
- [ ] Remove any `.gitkeep` files accidentally included.
- [ ] Add or check `cover_img:` license block (at the end of the post). Should include `image_url:`, `license:`, `license_url:`.
- [ ] Search for `available_in: [free, premium, ultimate]` and change to `available_in: [core, premium, ultimate]`.

In the review app:

- [ ] Check that images match the context in which they are used, and are clear.
- [ ] Check for duplicate entries.
- [ ] Search for the text `gitlab-ci.yml` and ensure there is a period before the filename, for example, `.gitlab-ci.yml`.
- [ ] Check that features introduced in this release do not mistakenly reference previous releases (this often happens after features slip to a future release after an RPI is already written). If, for example, the current release is 13.8, and an item reads: _"In GitLab 13.7 we introduced XXX..."_, this means the feature most likely slipped to 13.8. In that case, correct the text to _"In GitLab 13.8 we introduced XXX..."_. A search for two or three previous release numbers ("13.7", "13.6", and "13.5" in our example) in the review app should be enough to spot this.
- [ ] Check all dates mentioned in entries, ensuring they refer to the correct year.
- [ ] Check the anchor links in the intro. All links in the release post markdown file should point to something in the release post Yaml file.
- [ ] Run a dead link checker, e.g., [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf) and ping reporters directly on Slack asking them to fix broken links.
  - [ ] Links to confidential issues may be missed. It is helpful to check for broken links as an unauthenticated GitLab user (either logged out, in another browser, or in Incognito mode).
- [ ] Run a spelling check against the Release Post's View app. For example, using [Webpage Spell-Check](https://chrome.google.com/webstore/detail/webpage-spell-check/mgdhaoimpabdhmacaclbbjddhngchjik?hl=en) for Google Chrome.

In general:

- [ ] Report any problems from structural check in the `#release-post` channel by pinging the reporters directly for each problem. Do NOT ping `@all` or `@channel` nor leave a general message to which no one will pay attention. If possible, ensure open discussions in the merge request track any issues.
- [ ] Post a comment in the `#whats-happening-at-gitlab` channel linking to the View App + merge request reminding the team to take a look at the release post and to report problems in `#release-post`. CC the release post manager and messaging lead. Template to use (replace links):
  ```md
  Hey all! This month's release post is almost ready! Take a look at it and either
  report any problems in #release-post, or leave a comment to the release post MR.
  MR: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/1234
  View app: https://release-X-Y.about.gitlab-review.app/releases/YYYY/MM/DD/gitlab-X-Y-released/index.html
  ```
- [ ] Remove the label ~review-structure.
- [ ] Assign the MR to the next reviewer (release post manager)
- [ ] Notify release post manager that you're done with the structural check needed for **final content review**, by pinging them in the `Slack X-Y release post prep` channel.
- [ ] Within 1 week, update the release post templates and release post handbook with anything that comes up during the process.

#### Final content review (`@release_post_manager`)

**Due date: YYYY-MM-DD** (18th - 19th)

- [ ] Check to be sure there are no broken links in the View app (use a dead link checker, e.g., [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf)
- [ ] Mention `@sytse`, `@sfwgitlab`, and `@adawar` in #release-post on Slack when the post has been generated for their review per these [communication guidelines](https://about.gitlab.com/handbook/marketing/blog/release-posts/#communication)
- [ ] Capture any feedback from Slack into a single comment on the Release Post MR with action items assigned to the DRIs to address. More info [here](https://about.gitlab.com/handbook/marketing/blog/release-posts/#content-reviews)
- [ ] Request that the VP of Product Management `@adawar` identify the 3-7 items for **What's New ** by posting in Slack #release-post and linking him to [Creating an MR for What's New entries](https://about.gitlab.com/handbook/marketing/blog/release-posts/index.html#creating-an-mr-for-whats-new-entries) in the handbook. cc What's new DRIs Product operations '@fseifoddini` and Growth `@mkarampalas` in the Slack post.
     - This task should not happen prior to the 19th, in order to give sufficient time to be confident the features being released are fully confirmed. At the discretion of the release post manager, this task may be done on the 20th but no later. 

**Incorporating Feedback - Due date: YYYY-MM-DD** (by the 20th)

- [ ] Make sure all feedback from CEO and Product team reviews have been addressed by working with DRIs of those areas as needed
- [ ] If you receive feedback about the ordering Primary Items work with the Messaging Lead to determine how you might adjust the order.
- [ ] If applicable re-order Secondary items by adjusting the `titles` in the content blocks. More information to consider about altering secondary items [here](https://about.gitlab.com/handbook/marketing/blog/release-posts/#content-reviews) | [technical instructions](https://about.gitlab.com/handbook/marketing/blog/release-posts/#feature-order)
- [ ] Make sure there are no open feedback items in this MR or in Slack #release-post channel
- [ ] On 20th, ping Product Operations (`@fseifoddini`) for final check in Slack #release-post
- [ ] After VP Product Management review, remove the label ~review-in-progress

### Preparing to merge to master (`@release_post_manager`)
#### On the 21st

- [ ] Make sure the [homepage card](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/sites/uncategorized/source/includes/home/ten-oh-announcement.html.haml) was updated by the Messaging lead on the release X-Y branch.
- [ ] Check with the Messaging lead to confirm the social sharing copy is finalized (should be visible in View app at this point)
- [ ] Mention `@community-team` on Slack #swag to ask them to send the swag pack to the MVP
- [ ] Check if all the anchor links in the intro are working
- [ ] Confirm there are no broken links in the View app (use a dead link checker, e.g., [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf))
- [ ] Check the total features count statement in the introductory paragraph to make sure the number stated is accurate, and if not, update it and inform the Messaging lead. To get the number, you'll do a hand count of just features (do not include bugs, performance improvements, upgrades, etc.) in `/data/release_posts/X_Y` on the current `release-X-Y ` branch.
- [ ] Work with your [Technical Advisor](https://about.gitlab.com/handbook/marketing/blog/release-posts/#technical-advisors) to merge `master` into your release post branch before the 22nd. This prevents having to resolve conflicts when merging the release post branch into `master` on the 22nd. This issue shows an example of how this process can be handled: https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10886.
- [ ] Check to make sure all unresolved threads on this MR are resolved and there are no merge conflicts. If you need help resolving merge conflicts or other technical problems, ask for help from the [Technical Advisor](https://about.gitlab.com/handbook/marketing/blog/release-posts/#technical-advisors) in #dev-escalation channel in Slack then cross-post to #release-post channel to make others aware.
- [ ] Reach out to the [release managers (https://about.gitlab.com/community/release-managers/) the day before the release to let them know you are running the release and ask them to
keep you in the loop on the release.

#### On the 22nd (`@release_post_manager`)

#### At 12:30 UTC

- [ ] Read the [important notes](#important-notes) below
- [ ] Say hello in  `#releases` slack channel to let the release managers you're online and await their que in `#release-post` to start the merge process of the release post.
     - Release Managers will alert you in `#release-post` if there are any issues with the release. You can follow along on the release issue to see the packaging progress on the 22nd | [issue list](https://gitlab.com/gitlab-org/release/tasks/-/issues/) [example issue](https://gitlab.com/gitlab-org/release/tasks/-/issues/1261). The `#releases` slack channel is also a good place to track any updates or announcements.
  - If anything is wrong with the release, or if it's delayed, you must ping
  the messaging lead on `#release-post` so that they coordinate anything scheduled
  on their side (e.g., press releases, other posts, etc).
  - If everything is okay, the packages should be published at [13:30 UTC](https://gitlab.com/gitlab-org/release-tools/-/blob/fac347e5fc4e1f31cffb018d90061ef4f25747f3/templates/monthly.md.erb#L104-125), and available publicly around 14:10 UTC.
- [ ] Check to make sure there aren't any alerts on Slack `#release-post` and `#whats-happening-at-gitlab` channels
- [ ] Check to make sure there aren't any alerts on this MR or merge conflicts

### Merging to master (`@release_post_manager`)
#### At 13:50 UTC

Once the release manager confirmed that the packages are publicly available by pinging you in Slack:

- [ ] Announce to the team in #release-post that you are starting the final merge process and will reach out for help if the MR fails and that you will lead collaboration with the appropriate team members to resolve problems
     - Depending on the complexity of the failure it is recommended that you first try to resolve the issue yourself and then reach out to a #dev-escalation per [What to do if your pipeline fails or you have other technical problems](#What-to-do-if-your-pipeline-fails-or-you-have-other-technical-problems)
- [ ] Merge the MR at 14:10-14:20 UTC.
- [ ] Wait for the pipeline. This can take anywhere from 20-45 minutes to complete.
- [ ] Check the live URL on social media (after MR is merged) with [Twitter Card Validator](https://cards-dev.twitter.com/validator) and [Facebook Debugger](https://developers.facebook.com/tools/debug/). You may get a warning from Facebook that says "Missing Properties - The following required properties are missing: fb:app_id" - this can be ignored.
- [ ] Check for broken links again once the post is live.
- [ ] Handoff social posts to the social team and confirm that it's ready to publish: Mention @social in the `#release-post` Slack channel; be sure to include the live URL link and social media copy (you can copy/paste the final copy from the View app).
     - A member of the social team will schedule the posts at the next available best time on the same day. The social team will mark the Slack message with a ⏳ once scheduled and add scheduled times to the post thread for team awareness. Further details are listed below in the Important Notes Section.
- [ ] Share the links to the post on the `#release-posts` and `#whats-happening-at-gitlab` channels on Slack.

#### What to do if your pipeline fails or you have other technical problems

For assistance related to failed pipelines or eleventh-hour issues merging the release post, reach out to release post [technical advisors](https://about.gitlab.com/handbook/marketing/blog/release-posts/#technical-advisors) for assistance in the `#dev-escalation` Slack channel. Cross-post the thread from #dev-escalation in #release-post so all Product Managers and release post stakeholders are aware of status and delays.

#### Important notes

- The post is to be live on the **22nd** at **15:00 UTC**. It should
be merged and as soon as GitLab.com is up and running on the new
release version (or the latest RC that has the same features as the release),
and once all packages are publicly available. Not before. Ideally,
merge it around 14:20 UTC as the pipeline takes about 40 min to run.
- The usual release time is **15:00 UTC** but it varies according to
the deployment. If something comes up and delays the release, the
release post will be delayed with the release.
- Coordinate the timing with the [release managers](https://about.gitlab.com/community/release-managers/). Ask them to
keep you in the loop. Ideally, the packages should be published around
13:30-13:40, so they will be publicly available around 14:10 and you'll
be able to merge the post at 14:20ish.
- Once the release post is live, wait a few minutes to see if no one spots an error (usually posted in #whats-happening-at-gitlab or #company-fyi), then follow the `handoff to social team` checklist item above.
- The tweet to share on Slack will not be live, it will be scheduled during a peak awareness time on the 22nd. Once the tweet is live, the social team will share the tweet link in the `#release-post` and in the `#whats-happening-at-gitlab` Slack channels.
- Keep an eye on Slack and in the blog post comments for a few hours
to make sure no one found anything that needs fixing.



/label ~"blog post" ~release ~"release post" ~"priority::1"
/assign me
