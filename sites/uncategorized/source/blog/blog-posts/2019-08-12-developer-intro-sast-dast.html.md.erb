---
title: "Why you need static and dynamic application security testing in your development workflows"
window_title: "SAST vs DAST: Whats the difference? | GitLab"
author: Vanessa Wegner
author_gitlab: vwegner
author_twitter: gitlab
categories: insights
image_title: '/images/blogimages/intro-developer-sast-dast.jpg'
description: "Bolster your code quality with static and dynamic application security testing. Read here on why you need SAST and DAST for your projects."
tags: testing, security, CI/CD
twitter_text: "What are static and dynamic application security testing? Learn all about it"
postType: content marketing
---

DevOps is a quickly growing practice for companies in almost every market. With
the influx of cyber attacks over the past decade, security has slowly crept
forward in the SDLC to the point where we’re now hearing the term [DevSecOps](/blog/2019/06/20/announcing-gitlab-devsecops/) in developer circles.

To keep things tidy and help developers manage additional security
responsibilities, tools for static and dynamic [application security](/topics/application-security/) testing
(SAST and DAST) have made their way into the fray. In this post, we’ll
explain what SAST and DAST are, how they fit into developers’ workflows, and
when they should be used.

## What are SAST and DAST?

These are critical tools for successful DevSecOps. Each runs a set
of automated tests, and both introduce security at the beginning of the
software development lifecycle.

### Static application security testing

[SAST](https://docs.gitlab.com/ee/user/application_security/sast/) can
be used to analyze source code for known vulnerabilities – and is also a type
of white box testing. The test will run before your code is deployed, ensuring
that developers are alerted to fixes during the development phase.
SAST can help remediate situations where your code has a potentially dangerous
attribute in a class or unsafe code that can lead to unintended code execution.

![An example of a SAST summary within a GitLab merge request](/images/secure/sast.png){: .shadow.medium.center}

Within GitLab, SAST will automatically generate a summary of fixes and unresolved
vulnerabilities following every code commit, but before your code is merged to the target
branch. Tools that allow SAST reports to sit within the developer’s work
interface enable ease of remediation and streamline testing procedures within
the development phase.

### Dynamic application security testing

[DAST](https://docs.gitlab.com/ee/user/application_security/dast/), a
type of black box testing, analyzes your running web applications or known
runtime vulnerabilities. GitLab’s DAST tool runs live attacks on a review app
during QA, meaning developers can iterate on new apps and updates earlier and
faster.

As with SAST, DAST should auto-run so that the developer doesn’t have to take measures to initiate the test. In other situations, DAST can also be used to
continuously monitor live web applications for issues like cross-site scripting
or broken authentication flaws. Test results should inform developers of
potential vulnerabilities and serve as a catalyst for ongoing updates.

## Test early and often using SAST and DAST

Static and dynamic application security testing are two helpful tools to keep
your code secure, but don’t rely on them to handle all of your security needs.
It’s still important to do manual code reviews, test high-level behaviors and
functionality, conduct database scanning, and ensure that your whole team is
operating with a security-first mindset.

Cover image by [Mikael Kristenson](https://unsplash.com/@mikael_k?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
on [Unsplash](https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
{: .note}
