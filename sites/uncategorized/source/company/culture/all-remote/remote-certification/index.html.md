---
layout: handbook-page-toc
title: GitLab All-Remote Certification (Remote Work Foundation)
canonical_path: "/company/culture/all-remote/remote-certification/"
description: Get certified in GitLab's remote work foundations
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"

---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction 

![GitLab all-remote team](/images/all-remote/GitLab-All-Remote-Zoom-Team-Tanuki.jpg){: .shadow.medium.center}

GitLab is a pioneer in the all-remote space. As one of the largest all-remote companies in the world, we have developed a custom certification program to test and apply knowledge attained throughout the [all-remote](/company/culture/all-remote/) section of the handbook. Emerging leaders must quickly learn and deploy remote-first practices and skills while expanding their knowledge of remote management and overall remote fluency. 

Presented at a self-directed pace, the GitLab remote certification is designed to give [new managers](/company/culture/all-remote/being-a-great-remote-manager/) and [individual contributors](/company/culture/all-remote/getting-started/) an opportunity to master all-remote business concepts and build key skills in remote subject areas. 

Participants will gain a point of view on all-remote through [self-paced learning](/company/culture/all-remote/self-service/) and reading. 

## Certification benefits

1. Improve your remote fluency
1. Build organization, team management, and networking skills in a remote setting
1. Understand how GitLab has built and scaled one of the largest all-remote companies
1. Develop a framework for your own organization
1. Gain full transparency into the processes, policies, and procedures GitLab uses to be successful

## Who should complete the certification?

This certification is ideal for aspiring or new managers, individual contributors, or anyone who is looking to rapidly increase their remote foundational skills and strategic perspective. The certification is also ideal for individuals who prefer to complete training at their own pace through reading comprehension and viewing video. 

## Remote Work Foundation certification criteria

![GitLab all-remote team illustration](/images/all-remote/gitlab-com-all-remote-1280x270.png){: .shadow.medium.center}

In order to attain the **Remote Work Foundation** certification from GitLab, an individual must pass all ten knowledge assessments in the [GitLab Learn Remote Foundations Pathway](https://gitlab.edcast.com/pathways/copy-of-remote-foundations-badge) with a score of 80% or higher.

Once you have completed the certification in GitLab Learn, you will recieve the Remote Foundations Badge that you can share on your GitLab Learn and LinkedIn profile.

## More GitLab certifications

![GitLab values illustration](/images/all-remote/gitlab-values-tanukis.jpg){: .medium.center}

GitLab is actively building new certifications. Learn more about [current and upcoming certifications](/handbook/people-group/learning-and-development/certifications/) within the Learning & Development handbook. 

## Questions

If you have questions, please reach out to our [Learning & Development team](/handbook/people-group/learning-and-development/) at `learning@gitlab.com`.

----

Return to the main [all-remote page](/company/culture/all-remote/).

