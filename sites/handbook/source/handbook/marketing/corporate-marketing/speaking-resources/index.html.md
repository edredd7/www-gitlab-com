---
layout: handbook-page-toc
title: 'GitLab Speakers Resources'
---

The Corporate Marketing and Developer Evangelism teams want to enable everyone to contribute and spread the message about GitLab, DevOps, and cloud native to the entire world. We are happy to help speakers in any way we can, including public speaking coaching and building out slides. Below you can find some resources for becoming a speaker, finding an event, submitting a talk, and putting together the presentation and speech.

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Becoming a Speaker
So you're interested in becoming a public speaker? That's great!

1. Join the [GitLab Speakers Bureau](/speakers/), see the [Developer Evangelist page on the Speakers Bureau](/handbook/marketing/community-relations/developer-evangelism/speakers-bureau/).
1. Join other speakers bureaus relevant to your expertise.  For instance, if you're interested or involved in cloud native, Kubernetes, and the CNCF, apply to join the [CNCF speakers bureau](https://www.cncf.io/speakers/register/).
1. If you're looking to learn how to be a more effective and confident public speaker, you can check out:
 - [LinkedIn Learning courses](https://www.linkedin.com/learning/public-speaking-foundations-2018/)
  - Books like _The Art of Public Speaking_ by Dale Carnegie
  - Or join your local [Toastmasters](https://www.toastmasters.org/Resources/Public-Speaking-Tips).

## Get help developing CFP ideas

Sometimes people have an awesome story to share but can't find the time to put words on paper. [Share your ideas (Google Form)](https://forms.gle/5RbekekqWD1e6F686) and the Developer evangelism team will work with you to build it into a talk. In the event that you are unable to speak on the topic, we can get other speakers to do justice to the talk.

The submission is open to everyone including community members. If you have any question, please ask in the #dev-evangelism channel and mention `@abuango` (or [Twitter](https://twitter.com/sarki247)) in any ongoing GitLab issue. 

## Speaker Resources

### Finding an Event

- If you are interested in finding out about speaking opportunities, join the #cfp Slack channel.
- You can also see CFPs we're tracking with the `CFP` label [in the Corporate Marketing project](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=CFP)
- For a complete list of events we are tracking, see our [Events Spreadsheet](https://docs.google.com/spreadsheets/d/1KX8uf-4Ov8ybztJibQlGr9HvgH9VobpA8Nv5ecny1N4/edit#gid=1294176754)
- For even more conferences and CFPs, you can checkout [CFP Land](https://www.cfpland.com/)

### CFP Submission

- If you want help building out a talk, coming up with ideas for a speaking opportunity, or have a customer interested in speaking, start an issue in the marketing project:
 - Use the [CFP submissions template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=CFPsubmission)
  - Be sure to tag any associated event issues.
  - Complete as much info as possible, and we will ping you with the next steps.
- For ideas to help you or your customers get their submissions accepted, see [How to Get Your Presentation Accepted (video)](https://www.youtube.com/watch?v=wGDCavOCnA4) or schedule a chat with a [Developer Evangelism](/handbook/marketing/community-relations/developer-evangelism/) team member.

#### Process

If there is an event you would like to attend, are attending, speaking, or have proposed a talk and you would like support from GitLab to attend this event, the process goes as follows:

1. Contact your manager for approval to attend/ speak.
1. After getting approval from your manager to attend, [add your event/ talk](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/events.yml) to the [events page](/events/) and submit the merge request to Brendan O'Leary.

1. If the conference does not cover your travel and expenses, GitLab will cover your expenses (transportation, meals, and lodging for days said event takes place). If those expenses exceed $500, please get approval from your manager. When booking your trip, use our travel portal, book early, and spend as if it is your own money. Note: Your travel and expenses will not be approved until your event/engagement has been added to the events page.
1. If you are speaking, please note your talk in the description when you add it to the Events Page.
1. If you are not already on the [speakers page](/speakers/), please [add yourself](/handbook/marketing/community-relations/developer-evangelism/speakers-bureau/#join-the-speakers-bureau).
1. We suggest bringing swag and/or stickers with you. See notes on #swag on this page for info on ordering event swag.

### Presentation Assets
For presentation assets like templates and slides, see [the Strategic Marketing handbook](/handbook/marketing/strategic-marketing/sales-resources/). - Consider using a [GitLab branded Zoom background](https://docs.google.com/presentation/d/1PM4sCuCTSmVtoCp_O-_K9BS7yrIJjh1kteMTT7PS9zI/edit#slide=id.gc6bdfa0016_0_2) during your presentation to show your GitLab pride!

### Speakers Lean Coffee
Once a month, on the second Thursday of every month, the GitLab Developer Evangelism team hosts a Speakers Lean Coffee meeting in the [lean coffee](https://leancoffee.org/) style.  This means that prospective or current speakers who are GitLab team members or in the wider GitLab community can come and bring their topics to discuss.  Whether it is help in brainstorming ideas for a CFP, refining an abstract, or discussing an upcoming presentation in more detail - all topics are welcome.

To view or add topics for the next Speakers Lean Coffee, see the [Speakers Lean Coffee issue board](https://gitlab.com/gitlab-com/marketing/corporate_marketing/developer-evangelism/speakers-lean-coffee/-/boards).  You can find the invite on the [Developer Evangelism Calendar](https://calendar.google.com/event?action=TEMPLATE&tmeid=NnQ2aTRrZHIzbW4zaDhsbDY3MG9xamd1YXFfMjAyMTA2MTBUMTQwMDAwWiBnaXRsYWIuY29tX2V0YTdvNHRuNGJ0bjhoMGY4ZWlkNXE5OHJvQGc&tmsrc=gitlab.com_eta7o4tn4btn8h0f8eid5q98ro%40group.calendar.google.com&scp=ALL).

### After your Talk

If your talk is recorded, we encourage speakers to publish their talks to YouTube. Speakers should [upload their talks to GitLab Unfiltered](/handbook/marketing/marketing-operations/youtube/#uploading-conversations-to-youtube) or, if published elsewhere on YouTube, [add the recording](https://support.google.com/youtube/answer/57792) to the `GitLab Tech Talks` playlist on GitLab Unfiltered.

### Best Practices

Below are some tips on being a better presenter. For an in-depth book that covers the entire speaking process, from submitting an abstract through preparing a structured talk to practicing and delivering read [Demystifying Public Speaking](https://abookapart.com/products/demystifying-public-speaking).

1. Use a problem/solution format to **tell a story**. Many talks, especially tech talks, talk about what they built first and then what the result was. Flip this around and [start with the why](https://www.ted.com/talks/simon_sinek_how_great_leaders_inspire_action). Why did you need to take the action that you did? Talking about what problems you were encountering creates a narrative tension, and people will listen intently to the talk because they want to hear the solution.
1. **Drive towards an action**. Ask yourself, "What will people do once they hear this talk?" The answer can't be, "be more aware of this topic." By deciding what action you expect the audience to take, you can build your talk to drive towards this action. Talks that motivate the audience to action are more engaging and memorable than talks that simply describe. Some good example answers are
   1. Contribute to an open source project.
   1. Implement the technology or process you've come up with
   1. Follow the best practices you've outlined
1. **Practice how you play**. Practicing your talk is key to being a great presenter. As much as possible, practice exactly how you plan to give the talk. Stand up and pretend you are on stage rather than sitting down. If you'll demo, build the demo first and practice the demo. Even practicing while wearing the outfit you plan to wear can help.
1. **Give concrete examples**. Real life details bring a talk to life. Examples help people to understand and internalize the concepts you present. For each of your points, try to have a "for instance." As an example, "We recommend using this script to delete old logs and free up diskspace. For instance, one time our emails lit up as users were complaining about slow performance. Some were reporting tasks hanging for over an hour when they should have completed in less than a minute. It turned out we were out of diskspace because we had verbose logging enabled. Once we ran the script we saw performance return to normal levels."
1. **Be mindful of your body language** when presenting as it will impact the way the audience perceives your presentation. Move around the stage purposefully (don't pace or fidget). Make natural gestures with your hands, and maintain good posture to convey confidence and openness, which will help you to better connect with your audience.

## Finding a Speaker

To find a speaker for your event, you can see the following resources:

- Speakers Bureau: a catalogue of talks, speaker briefs and speakers can be found on our [Speakers Bureau page](/speakers/). Feel free to add yourself to this page and submit a MR if you want to be in our speaker portal and are interested in being considered for any upcoming speaking opportunities.

